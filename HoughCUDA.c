/*
* Performs Hoght transdform given image in the
* parameters uing CUDA. To keep things simple
* file includes copy pasted functions for
* handling PNG files.
*/
#include<getopt.h>
#include<unistd.h>
#include<fcntl.h>
#include<sys/stat.h>
#include<string.h>
#include<math.h>
#include<cuda.h>
#include<stdio.h>

// Enum for selecting color type.
// COLOR_TYPE_NULL should be used
// when color type is marked unknow. 
enum ColorType{
	COLOR_TYPE_GRAY,
	COLOR_TYPE_GRAYALPHA,
	COLOR_TYPE_RGB,
	COLOR_TYPE_NULL=100
};
// Simple structure that such has
// information about the PNG image.
// It doesn't store the image data.
//
// Members:
//   width is number of the columns
//   height is number of rows.
//   colortype RGBA or something else.
//   bitdepth is number bits per
//     channel.
//   special is weird one. It
//     stores inside of it result
//     from png_get_valid which
//     answer for example is image
//     sRGB or not.
typedef struct{
	uint32_t width;
	uint32_t height;
	uint32_t special;
	uint32_t allocationsize;
	enum ColorType colortype;
	int bitdepth;
	int interlace;
	int compression;
	int filter;
	int rowsize;
}ImageInfo;


// This function reads from the file and 
// uses it generate image. Image generated
// has notting to do with what file actual
// repesent (PNG file not going to be image
// of the image stored in file).
int fileImageRead(ImageInfo *info,CUdeviceptr *gpumemory,const char *filepath){
	
	int fd=open(filepath,O_RDONLY);
	if(fd>-1){

		// Query size of the file by
		// getting "inode" of the file.
		struct stat inode;
		fstat(fd,&inode);

		// Buffer and variable how much buffer was used
		// for reading from the file.
		uint8_t readbuffer[4096];
		uint32_t n;
		
		if((n=read(fd,readbuffer,sizeof(readbuffer)))>0){		
	
			// Read first two bytes as size of the image.
			info->width=readbuffer[0];
			info->height=readbuffer[1];

			// Check that there is enough of file.
			// Plus two is because we already used two
			// bytes of memory.
			info->allocationsize=info->width*info->height;
			if(inode.st_size>=info->allocationsize+2){

				// Allocate the image
				if(cuMemAlloc(gpumemory,info->allocationsize)==CUDA_SUCCESS){

					if(info->allocationsize>n-2){
						if(cuMemcpyHtoD(*gpumemory,readbuffer+2,n-2)!=CUDA_SUCCESS){
							(void)write(STDERR_FILENO,"ERROR: cuMemCpy 1.1 | fileImageRead\n",36);
							goto jmp_MEMCPY_ERROR;
						}

						// Read the file and directly copy it to gpumemory.
						uint32_t size=0;
						while((n=read(fd,readbuffer,sizeof(readbuffer)))>0){
							if(info->allocationsize-size>n){
								if(cuMemcpyHtoD(*gpumemory,readbuffer,n)!=CUDA_SUCCESS){
									(void)write(STDERR_FILENO,"ERROR: cuMemCpy 2.1 | fileImageRead\n",36);
									goto jmp_MEMCPY_ERROR;
								}
								size+=n;
							}
							else{
								if(cuMemcpyHtoD(*gpumemory,readbuffer,info->allocationsize-size)!=CUDA_SUCCESS){
									(void)write(STDERR_FILENO,"ERROR: cuMemCpy 2.2 | fileImageRead\n",36);
									goto jmp_MEMCPY_ERROR;
								}
								break;
							}
						}
					}
					else if(cuMemcpyHtoD(*gpumemory,readbuffer+2,info->allocationsize)!=CUDA_SUCCESS){
						(void)write(STDERR_FILENO,"ERROR: cuMemCpy 1.2 | fileImageRead\n",36);
						goto jmp_MEMCPY_ERROR;
					}

					// Add addition information.
					info->rowsize=info->width;
					info->colortype=COLOR_TYPE_GRAY;
					info->bitdepth=8;
			
					info->special=0;
					info->interlace=0;
					info->compression=0;
					info->filter=0;

					close(fd);
					return 1;
		
				}
			}
			else (void)write(STDERR_FILENO,"ERROR: file didn't have enough bytes. | fileImageRead\n",54);
		
			// If cuMemcpyHtoD fail in this function they jump here!
			jmp_MEMCPY_ERROR:
			
			close(fd);
		}
	}
	else (void)write(STDERR_FILENO,"ERROR: open | fileImageRead\n",28);

	return 0;
}
// Writes the gpumemory location to file 
// (given by filepath) as simple ascii art.
int Asciiwrite(const ImageInfo *info,const CUdeviceptr gpumemory,const char *filepath){

	// If everything goes well result should be
	// set 1 marking the success.
	int returner=1;
	int fd;
	if(filepath) fd=open(filepath,O_CREAT | O_TRUNC | O_WRONLY,S_IRUSR | S_IWUSR);
	else fd=STDOUT_FILENO;
	if(fd>-1){

		uint8_t *cpybuffer=malloc(info->allocationsize);
		cuMemcpyDtoH(cpybuffer,gpumemory,info->allocationsize);
		
		// Total length of ascii buffer
		uint32_t asciibufferlen=info->width*info->height*20+info->height;;
		// Asciibuffer that is written to the file
		char *asciibuffer=malloc(asciibufferlen);
		// Index in ascii buffer.	
		uint32_t bufferindex=0;

		switch(info->bitdepth){
		
		// Just write grayscale to memory.
		case 8:
		
			// Set buffer to ascii to H symbol and
			// set color based upon byte from gpu memory.
			for(uint32_t x=0;x<info->width;x++){
				for(uint32_t y=0;y<info->height;y++){
					// CSI command format ESC [ which is same 27 and 91.
					// Then to get 24 bit color use 38 foreground and 2.
					uint8_t byte=cpybuffer[y*info->width+x];			
				
					bufferindex+=sprintf(asciibuffer+bufferindex,"\x1b[38;2;%hhu;%hhu;%hhumH",byte,byte,byte);
				}
				asciibuffer[bufferindex++]='\n';
			}
			break;

		// Since true color is 24bits
		// we have to normalize the
		// data.
		case 32:
			{
				// Alias cpybuffer to 32 bit integrals				
				uint32_t *cpybuffer32=(uint32_t*)cpybuffer;
				// Length of cpybuffer32.
				uint32_t cpybuffer32len=info->width*info->height;

				// Calculate maximum of copybuffer32.				
				uint32_t max=0;
				for(uint32_t index=0;index<cpybuffer32len;index++){
					if(max<cpybuffer32[index]) max=cpybuffer32[index];
				}

				// Normize so that max is 16777216=2^24.
				// Formula used is floor(cpybuffer/max*2^24)
				for(uint32_t index=0;index<cpybuffer32len;index++){
					// This has to be 64 bit integer so that
					// number doesn't overflow. Max in 
					// copybuffer is 2^32 which is then
					// multiplied by 2^24 making maximun 
					// 2^56 which fits to 64 bit integer.
					uint64_t temp=cpybuffer32[index]*16777216;
					// Now divade by max and we get the number
					// on 24 bit range.
					cpybuffer32[index]=(uint32_t)(temp/max);
				}

				// Populate the ascii buffer.
				for(uint32_t x=0;x<info->width;x++){
					for(uint32_t y=0;y<info->height;y++){
						bufferindex+=sprintf(asciibuffer+bufferindex
						                    ,"\x1b[38;2;%hhu;%hhu;%hhumH"
						                    ,(cpybuffer32[y*info->width+x]&0xFF0000)>>16
						                    ,(cpybuffer32[y*info->width+x]&0x00FF00)>>8
						                    ,(cpybuffer32[y*info->width+x]&0x0000FF)>>0
						                    );
					}
					asciibuffer[bufferindex++]='\n';
				}

			}
			break;
		}
		if(write(fd,asciibuffer,bufferindex)<1){
			(void)write(STDERR_FILENO,"ERROR: write | Asciiwrite\n",26);
			returner=0;		
		}
		free(asciibuffer);
		
		close(fd);
	}
	else returner=0;

	return returner;
}
// Main entry to the program.
// Does initialiation and GPU ordering.
//
// Commandline arguments:
//  Non-options are images to be hough
//    tranformed. For now have to PNGs.
//  -g index selecs the GPU of given index.
//  -e threshold selects threshold used in
//    edge detection.
//	-a .
//  -r .
int main(int argn,char **args){

	// Initialization function for CUDA.
  // Flag is zero since it has to be!
	if(cuInit(0)==CUDA_SUCCESS){

		// Which GPU to use. Defaults to first one.
		int selectedgpu=0;
		// Number of GPU in the system.
		int numberofgpus=0;
		// Edge detection threshold
		float edgethreshold=0.2;
		// Number of angle and radius ticks
		uint16_t angleticks=0;
		uint16_t radiusticks=0;
		// File output is directed.
		char *fileoutput=0;

		if(cuDeviceGetCount(&numberofgpus)==CUDA_SUCCESS){

			// Handle Arguments with getopt.
			// For simplicity don't use long options.
			{
				int c;
				while((c=getopt(argn,args,"a:e:g:r:o:"))!=-1){
					switch(c){
						case 'a':
							angleticks=(uint16_t)atoi(optarg);
							break;
						case 'r':
							radiusticks=(uint16_t)atoi(optarg);
							break;
						case 'e':
							edgethreshold=atof(optarg);
							break;
						case 'g':
							selectedgpu=atoi(optarg);
							break;
						case 'o':
							fileoutput=malloc(strlen(optarg)+1);
							strcpy(fileoutput,optarg);
							break;
					}
				}
			}

			// Variable to select what image reader and writer
			// proram uses.
			int (*reader)(ImageInfo *info,CUdeviceptr *gpumemory,const char *filepath)=fileImageRead;
			int (*writer)(const ImageInfo *info,const CUdeviceptr gpumemory,const char *filepath)=Asciiwrite;

			// Get handler for GPU to be used
			// for this hought transform. Make
			// sure that selectedgpu is less
			// the numberofgpus.
			if(selectedgpu<numberofgpus){
				CUdevice gpu;
				if(cuDeviceGet(&gpu,selectedgpu)==CUDA_SUCCESS){

					// Maximum number threads.
					int maxthreads;
					cuDeviceGetAttribute(&maxthreads,CU_DEVICE_ATTRIBUTE_MAX_THREADS_PER_BLOCK,gpu);
					// Maximum number of threads in x dimension.
					int maxblockdimx;
					cuDeviceGetAttribute(&maxblockdimx,CU_DEVICE_ATTRIBUTE_MAX_BLOCK_DIM_X,gpu);
					// Maximum number of threads in y dimension.
					int maxblockdimy;
					cuDeviceGetAttribute(&maxblockdimy,CU_DEVICE_ATTRIBUTE_MAX_BLOCK_DIM_Y,gpu);
					// Maximum number of blocks in x dimemsion.
					int maxgriddimx;
					cuDeviceGetAttribute(&maxgriddimx,CU_DEVICE_ATTRIBUTE_MAX_GRID_DIM_X,gpu);
					// Maximum number of blocks in y dimemsion.
					int maxgriddimy;
					cuDeviceGetAttribute(&maxgriddimy,CU_DEVICE_ATTRIBUTE_MAX_GRID_DIM_Y,gpu);
					// Maximum amount of shared memory per block in bytes.
					int maxshared;
					cuDeviceGetAttribute(&maxshared,CU_DEVICE_ATTRIBUTE_MAX_SHARED_MEMORY_PER_BLOCK,gpu);
					// Amount of threads in warp (number of threads executed simultaneously).
					int warpsize;
					cuDeviceGetAttribute(&warpsize,CU_DEVICE_ATTRIBUTE_WARP_SIZE,gpu);
			
					// Number of processor
					int gpuprocessors;
					cuDeviceGetAttribute(&gpuprocessors,CU_DEVICE_ATTRIBUTE_MULTIPROCESSOR_COUNT,gpu);

					// Create context for GPU cpu interaction.
					CUcontext context;
					if(cuCtxCreate(&context,CU_CTX_SCHED_SPIN,gpu)==CUDA_SUCCESS){

						// Load the "shared library" which has GPU code and
						// "get address" to functions.
						CUmodule libhough;
						if(cuModuleLoad(&libhough,"libhough.fatbin")==CUDA_SUCCESS){
							CUfunction rgbtograykernel;
							if(cuModuleGetFunction(&rgbtograykernel,libhough,"rgbToGray")==CUDA_SUCCESS){
								CUfunction sobelkernel;
								if(cuModuleGetFunction(&sobelkernel,libhough,"sobel")==CUDA_SUCCESS){
									CUfunction houghlinekernel;
									if(cuModuleGetFunction(&houghlinekernel,libhough,"houghLine")==CUDA_SUCCESS){

										// LOAD THE IMAGE LOOP
										// Loop for over every PNG image.
										// Loop goes through all the
										for(char *file=args[optind];optind<argn;file=args[++optind]){
											ImageInfo imageinfo;
											CUdeviceptr image;
											if(reader(&imageinfo,&image,file)){

												// CALCULATE GRID AND BLOCK SIZE
												// TODO: What happens if image is too small.
												// TODO: What if image is so big there
												//       need to have loop inside of the GPU code?
												unsigned int blockx=warpsize;
												unsigned int blocky=warpsize;

												unsigned int gridx=imageinfo.width/blockx+(imageinfo.width%blockx>0);
												unsigned int gridy=imageinfo.height/blocky+(imageinfo.height%blocky>0);


												// Change color images to gray scale and continue with gray scale images.
												CUdeviceptr grayimage;
												switch(imageinfo.colortype){
													case COLOR_TYPE_RGB:
														if(cuMemAlloc(&grayimage,imageinfo.width*imageinfo.height)==CUDA_SUCCESS){

															// Execute RGB to Gray kernel to get gray.
															void *args[]={&image,&grayimage,&imageinfo.width,&imageinfo.height,0};

															if(cuLaunchKernel(rgbtograykernel,gridx,gridy,1,blockx,blocky,1,0,0,args,0)!=CUDA_SUCCESS){
																(void)write(STDERR_FILENO,"ERROR: cuLaunchKernel | grayimage\n",35);
																goto jmp_SAFE_EXIT_GRAYIMAGE;
															}

														}
														else{
															(void)write(STDERR_FILENO,"ERROR: cuMemAlloc | grayimage\n",30);
															goto jmp_SAFE_EXIT_GRAYIMAGE;
														}
														break;
													case COLOR_TYPE_GRAY:
														grayimage=image;
														break;
													default:
														fprintf(stderr,"\nSkipping the file \"%s\" since it has unimplemented colortype!\n",file);
														cuMemFree(image);
														continue;
												}

												// Run Egde detection. Output will be list of indexes (y*width+x)
												// and number memory allocated for that list is used.
												CUdeviceptr binedge;
												if(cuMemAlloc(&binedge,imageinfo.width*imageinfo.height*sizeof(uint8_t))==CUDA_SUCCESS){
													CUresult result;
													{
														void *args[]={&grayimage,&binedge,&imageinfo.width,&imageinfo.height,&edgethreshold,0};
														result=cuLaunchKernel(sobelkernel,gridx,gridy,1,blockx,blocky,1,0,0,args,0);
													}
													if(result==CUDA_SUCCESS){

														// Make edge list from grayimage manually on CPU since we have to
														// count up which isn't easy to do.
														// TODO: Design how to do this on GPU at edge detector!
														uint32_t *deviceedgelist=malloc(sizeof(uint32_t)*imageinfo.width*imageinfo.height);
														if(deviceedgelist){
															uint32_t edgelistcount=0;
															uint8_t *edgeimagedevice=malloc(sizeof(uint8_t)*imageinfo.width*imageinfo.height);
															if(edgeimagedevice){
																cuMemcpyDtoH(edgeimagedevice,binedge,sizeof(uint8_t)*imageinfo.width*imageinfo.height);
																// Don't need edgeimage anymore.
																cuMemFree(binedge);

																for(uint32_t x=0;x<imageinfo.width;x++){
																	for(uint32_t y=0;y<imageinfo.height;y++){
																		if(edgeimagedevice[y*imageinfo.width+x]>0) deviceedgelist[edgelistcount++]=(x<<16)+y;
																	}
																}

																// If we don't have enough edge pixels, then
																// there isn't much of point continuing.
																// Warpsize used as counter so that later
																// when calculating kernel grid sizes isn't
																// zero.
																// TODO: Better grid size calculation on hough transform.
																if(edgelistcount>warpsize*2){

																	CUdeviceptr edgelist;
																	if(cuMemAlloc(&edgelist,sizeof(uint32_t)*edgelistcount)==CUDA_SUCCESS){
																		cuMemcpyHtoD(edgelist,deviceedgelist,sizeof(uint32_t)*edgelistcount);
																		free(deviceedgelist);
																		free(edgeimagedevice);

																		// Run the hough transform.

																		// Calculate angle and distance difference used in accumulator.
																		// Angle should be between 0 and <PI>/2 and radius should be
																		// between zero and image diagonal (sqrt(width^2+height^2)).
																		// User can give tick values so check are they are given. 
																		// TODO: Think about effective calculating reasonable values.
																		float maxradius=sqrt(imageinfo.width*imageinfo.width+imageinfo.height*imageinfo.height);
																		float angled;
																		float radiusd;
																		if(angleticks>0) angled=(M_PI/2)/angleticks;
																		else{
																			angled=0.1;
																			angleticks=(uint16_t)floor((M_PI/2)/angled);
																		}
																		if(radiusticks>0) radiusd=maxradius/radiusticks;
																		else{
																			radiusd=1;
																			radiusticks=(uint16_t)floor(maxradius/radiusd);
																		}

																		// Allocate accumator based upon number ticks we have.
																		// Also memset to zero so that we have clean memory.
																		CUdeviceptr accumulator;
																		if(cuMemAlloc(&accumulator,sizeof(uint32_t)*angleticks*radiusticks)==CUDA_SUCCESS){
																			cuMemsetD32(accumulator,0,angleticks*radiusticks);
																			// Run thread per edge index.
																			// Use only dimension as input is index list.
																			{
																				void *args[]={&edgelist,&edgelistcount,&accumulator,&angled,&radiusd,&angleticks,&radiusticks,0};
																				result=cuLaunchKernel(houghlinekernel,(edgelistcount/warpsize)+(edgelistcount%warpsize>0),1,1,warpsize,1,1,0,0,args,0);
																			}
																			if(result==CUDA_SUCCESS){


																				// Make sure image to be written out is gray image with 8 bit channel.
																				imageinfo.width=angleticks;
																				imageinfo.height=radiusticks;
																				imageinfo.allocationsize=imageinfo.width*imageinfo.height*4;
																				imageinfo.rowsize=imageinfo.width*4;
																				imageinfo.colortype=COLOR_TYPE_RGB;
																				imageinfo.bitdepth=32;

																				writer(&imageinfo,accumulator,fileoutput);

																			}
																			else (void)write(STDOUT_FILENO,"ERROR: cuLaunchKernel | hough\n",30);
																		}
																	}
																}
																else{
																	free(deviceedgelist);
																	free(edgeimagedevice);
																	(void)write(STDOUT_FILENO,"No edge pixel found\n",20);
																}
															}
															else{
																free(deviceedgelist);
																(void)write(STDOUT_FILENO,"ERROR: malloc failled!\n",23);
															}
														}
														else (void)write(STDOUT_FILENO,"ERROR: malloc failled!\n",23);

													}
													else{
														cuMemFree(binedge);
														(void)write(STDERR_FILENO,"ERROR: cuLaunchKernel | sobelkernel\n",36);
													}
												}
												else (void)write(STDERR_FILENO,"ERROR: cuMemAlloc | binedge\n",28);

												// Program jumps here if rgbToGray
												// errors for some reason.
												jmp_SAFE_EXIT_GRAYIMAGE:
												// If image was already gray we don't
												// need to free gray version of it.
												if(grayimage!=image) cuMemFree(grayimage);

												cuMemFree(image);
											}
											else fprintf(stderr,"\nFile read error happened to \"%s\".\nProgram continues despite this!\n",file);
										}
									}
									else (void)write(STDERR_FILENO,"ERROR: Kernel | houghline\n",26);
								}
								else (void)write(STDERR_FILENO,"ERROR: Kernel | sobel\n",22);
							}
							else (void)write(STDERR_FILENO,"ERROR: Kernel | rgbToGray\n",26);

							// Unload the module
							cuModuleUnload(libhough);
						}
						else (void)write(STDERR_FILENO,"ERROR: cuModuleLoad | libhough\n",31);

						// Since loop is behind us just destroy the GPU context.
						cuCtxDestroy(context);
					}
					else (void)write(STDERR_FILENO,"ERROR: cuCtxCreate\n",19);
				}
				else (void)write(STDERR_FILENO,"ERROR: cuDeviceGet\n",20);
			}
			else (void)write(STDERR_FILENO,"ERROR: selectedgpu is more of equal to number GPUs\n",54);
		}
		else (void)write(STDERR_FILENO,"ERROR: cuDeviceGetCount\n",25);
	}
	else (void)write(STDERR_FILENO,"ERROR: cuInit\n",14);

	return 0;
}
